"""
def phone_hide(persons):
    hidden_persons = []
    for person in persons:
        name, phone = person.split(": ")
        parts = phone.split("-")
        hidden_phone = "***-***-" + parts[-1]
        hidden_persons.append(f"{name}: {hidden_phone}")
    return hidden_persons


def email_hide(persons):
    hidden_persons = []
    for person in persons:
        name, email = person.split(": ")
        parts = email.split("@")
        hidden_email = parts[0][0] + "***" + parts[0][-1] + "@" + "***" + parts[1]
        hidden_persons.append(f"{name}: {hidden_email}")
    return hidden_persons
"""
import re


def phone_hide(persons: list[str]) -> list[str]:
    return [re.sub(r"\d{3}-", "***-", person) for person in persons]


def email_hide(persons: list[str]) -> list[str]:
    return [re.sub(r": ([a-z])[a-z.]+([a-z])@[a-z]+([a-z])", r": \1***\2@***\3", person) for person in persons]