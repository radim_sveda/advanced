import re

def find_dates(text):
    # dd.mm.yyyy
    date_pattern = r"\b\d{1,2}\.\d{1,2}\.\d{4}\b"
    dates = re.findall(date_pattern, text)
    return dates

def test_find_dates():
    text = "Důležitý termín je 15.02.2023 a další je 20.6.2024."
    expected_dates = ["15.02.2023", "20.6.2024"]
    actual_dates = find_dates(text)

    assert actual_dates == expected_dates, "Chyba při vyhledávání dat."

test_find_dates()
